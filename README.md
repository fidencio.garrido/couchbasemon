# Couchbase Monitor

## Components

- Scrapper. Reads stats from API (Go)
- Admin. UI
- Reporting tools. (Go and Python)

**External**

- Couchbase. Stores performance stats.
- Nats. Connects scrapper with UI for push notifications.