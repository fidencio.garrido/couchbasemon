package main

import (
	"flag"
	"log"
	"runtime"
	"time"

	"github.com/google/uuid"
)

var port = flag.String("p", "8080", "Service port")
var config = flag.String("config", "./config.json", "Configuration file")
var scrapperID = flag.String("id", "Random UUID", "Monitor ID, used as part of the DB documents")
var help = flag.Bool("help", false, "Displays help information")
var clientTimeout = flag.Duration("t", time.Duration(3*time.Second), "Couchbase API timeout")
var maxProcs = flag.Int("c", 2, "Max parallelism")
var configuration Configuration
var monitorID = uuid.New().String()
var readOnlyMode = false

/*
VERSION Application version
*/
const VERSION = "1.0.0"

func startBroker() {

}

func main() {
	flag.Parse()
	if *help {
		flag.PrintDefaults()
	} else {
		if *maxProcs > runtime.NumCPU() {
			*maxProcs = runtime.NumCPU()
		}
		runtime.GOMAXPROCS(*maxProcs)
		log.Printf("Couchbase Monitor %s available in port %s. Using %d cores", VERSION, *port, *maxProcs)
		configuration = readConfig()
		if *scrapperID != "Random UUID" {
			monitorID = *scrapperID
		}
		log.Printf("Monitor ID %s", monitorID)
		connectDB()
		startBroker()
		startMonitor()
		startWebServer()
	}
}
