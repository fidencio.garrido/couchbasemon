# Scrapper

Gets Couchbase stats.

* Stores results in Couchbase Bucket
* Publishes to NATS
* Configurable interval per monitor
* Coordinate multiple monitor?

## Configuration example

```json
{
	"interval": 5000,
	"store":{
		"server": "localhost",
		"bucket": "default",
		"password": "",
		"rawStatsTTL": 3600
	},
	"messaging":{
		"server": "localhost"
	},
	"monitors": [
		{
			"active": true,
			"name": "myCluster",
			"cluster": "10.10.10.10",
			"password": "",
			"user": ""
		}
	]
}
```

## API

### Monitor
* ```GET /monitor``` Get list of monitors
* ```POST /monitor/{clusterId}``` Add monitor
* ```PUT /monitor/{clusterId}``` Update monitor
* ```PUT /monitor/{clusterId}/activate``` Activates monitor
* ```PUT /monitor/{clusterId}/deactivate``` Deactivates monitor
* ```GET /monitor/{clusterId}/toggle``` Toggle monitor state
* ```DELETE /monitor/{clusterId}``` Remove monitor

Monitor model

| Field | Type | Notes |
|---------|------|-------|
| cluster | String | One of the servers in the cluster without protocol |
| name | String | Name of the cluster (i.e. "Prod-West 1") |
| active | Boolean | Default true. Indicates if the monitor should be reading stats|
| password | String | Couchbase admin password, required to read stats|
| user | String | Couchbase user (Couchbase 5 only)|


| cpuSpike | Number | Default 15, max 99. CPU increment flagged as CPU spike|  
| interval | Number | Default 5000. Interval between stats sampling, in milliseconds |


### Snapshot
* ```POST /snapshot/``` Start snapshot
* ```PUT /snapshot/``` Stop snapshot

Snapshot

| Field | Type   | Notes |
|-------|--------|-------|
| name  | String | Defines a custom name for the session |
| clusterName | String | Identifies the cluster for the session |
| id | String | Required to stop a snapshot/session |