package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

/*
SnapshotInfo Structure used to persist statistics
*/
type SnapshotInfo struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Timestamp int64  `json:"time"`
	Cluster   string `json:"clusterName"`
}

func getMonitors(w http.ResponseWriter, r *http.Request) {
	resp, _ := json.Marshal(configuration.Monitors)
	w.Write(resp)
}

func snapshotCreate(w http.ResponseWriter, r *http.Request) {
	var snapshotRequest SnapshotInfo
	json.NewDecoder(r.Body).Decode(&snapshotRequest)
	ts := time.Now().Unix()
	snapshotRequest.ID = uuid.New().String()
	snapshotRequest.Type = "Start"
	snapshotRequest.Timestamp = ts
	saveStats(snapshotRequest.Name, "snapshot", snapshotRequest, ts)
	response, _ := json.Marshal(snapshotRequest)
	w.Write(response)
}

func snapshotStop(w http.ResponseWriter, r *http.Request) {
	var snapshotRequest SnapshotInfo
	json.NewDecoder(r.Body).Decode(&snapshotRequest)
	ts := time.Now().Unix()
	snapshotRequest.Type = "Stop"
	snapshotRequest.Timestamp = ts
	saveStats(snapshotRequest.Name, "snapshot", snapshotRequest, ts)
	response, _ := json.Marshal(snapshotRequest)
	w.Write(response)
}

func startWebServer() {
	log.Print("Starting web server")
	mux := mux.NewRouter()
	mux.HandleFunc("/monitor", getMonitors).Methods("GET")
	mux.HandleFunc("/snapshot", snapshotCreate).Methods("POST")
	mux.HandleFunc("/snapshot", snapshotStop).Methods("PUT")
	server := &http.Server{
		Addr:        "0.0.0.0:" + *port,
		Handler:     mux,
		ReadTimeout: 300 * time.Second,
	}
	server.ListenAndServe()
}
