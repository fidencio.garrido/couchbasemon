package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

/*
MonitorInfo Cluster configuration
*/
type MonitorInfo struct {
	Active   bool   `json:"active"`
	Cluster  string `json:"cluster"`
	Name     string `json:"name"`
	Password string `json:"password"`
	User     string `json:"user"`
	CPUSpike uint   `json:"cpuSpike"`
}

/*
StoreInfo Configuration of Couchbase bucket used to persist the stats, if the ```server``` is empty no data will be persisted
*/
type StoreInfo struct {
	Server      string `json:"server"`
	Bucket      string `json:"bucket"`
	Password    string `json:"password"`
	RawStatsTTL uint32 `json:"rawStatsTTL"`
}

/*
MessagingInfo NATS server configuration
*/
type MessagingInfo struct {
	Server string `json:"server"`
}

/*
Configuration Application main configuration
*/
type Configuration struct {
	Store     StoreInfo     `json:"store"`
	Interval  uint          `json:"interval"`
	Messaging MessagingInfo `json:"messaging"`
	Monitors  []MonitorInfo `json:"monitors"`
}

func readConfig() Configuration {
	var configb []byte
	var cfg Configuration
	fcontent, err := ioutil.ReadFile(*config)
	if err == nil {
		configb = fcontent
		json.Unmarshal(configb, &cfg)
	} else {
		log.Print(err)
		panic("Invalid service configuration")
	}
	return cfg
}
