package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

/*
NodeInfo Couchbase node summary
*/
type NodeInfo struct {
	Host        string   `json:"host"`
	MemoryUsed  int64    `json:"memoryUsed"`
	MemoryFree  int64    `json:"memoryFree"`
	MemoryTotal int64    `json:"memoryTotal"`
	Uptime      int64    `json:"uptime"`
	CPUUsed     float64  `json:"cpuUsed"`
	Version     string   `json:"version"`
	GetHits     int64    `json:"getHits"`
	Services    []string `json:"services"`
	IsQuery     bool     `json:"isQuery"`
	IsData      bool     `json:"isData"`
	IsIndex     bool     `json:"isIndex"`
}

/*
NodeSystemStatsResponse Couchbase node system stats
*/
type NodeSystemStatsResponse struct {
	CPUUtilizationRate float64 `json:"cpu_utilization_rate"`
	MemTotal           int64   `json:"mem_total"`
	MemFree            int64   `json:"mem_free"`
}

/*
InterestingStatsResponse Couchbase node bucket hits
*/
type InterestingStatsResponse struct {
	GetHits int64 `json:"get_hits"`
}

/*
ClusterNodesResponse Couchbase node basic information
*/
type ClusterNodesResponse struct {
	Services         []string                 `json:"services"`
	Hostname         string                   `json:"hostname"`
	Uptime           string                   `json:"uptime"`
	Version          string                   `json:"version"`
	InterestingStats InterestingStatsResponse `json:"interestingStats"`
	SystemStats      NodeSystemStatsResponse  `json:"systemStats"`
}

/*
ClusterResponse Array of Couchbase nodes
*/
type ClusterResponse struct {
	Nodes []ClusterNodesResponse `json:"nodes"`
}

/*
BucketStatsQuotaResponse Couchbase bucket stats
*/
type BucketStatsQuotaResponse struct {
	RAM    int64 `json:"ram"`
	RawRAM int64 `json:"rawRAM"`
}

/*
BucketStatsBasicStatsResponse Bucket basic stats
*/
type BucketStatsBasicStatsResponse struct {
	QuotaPercentUsed float32 `json:"quotaPercentUsed"`
	OpsPerSecond     int64   `json:"opsPerSec"`
	DiskFetches      int64   `json:"diskFetches"`
	Items            int64   `json:"itemCount"`
	DiskUsed         int64   `json:"diskUsed"`
	MemUsed          int64   `json:"memUsed"`
}

/*
BucketStatsResponse response
*/
type BucketStatsResponse struct {
	Name          string                        `json:"name"`
	Quota         BucketStatsQuotaResponse      `json:"quota"`
	BasicStats    BucketStatsBasicStatsResponse `json:"basicStats"`
	ThreadsNumber int32                         `json:"threadsNumber"`
	ReplicaNumber int32                         `json:"replicaNumber"`
}

/*
QueryAdminResponse Query specific stats
*/
type QueryAdminResponse struct {
	Threads                  int32   `json:"total.threads"`
	Cores                    int32   `json:"cores"`
	GCPausePercent           float32 `json:"gc.pause.percent"`
	RequestCompleted         int64   `json:"request.completed.count"`
	RequestActive            int64   `json:"request.active.count"`
	RequestPreparedStatement int64   `json:"request.prepared.percent"`
}

/*
ActiveQueriesResponse Active queries report
*/
type ActiveQueriesResponse struct {
	ElapsedTime      string `json:"elapsedTime"`
	ExecutionTime    string `json:"executionTime"`
	RequestStatement string `json:"request.statement"`
	RequestID        string `json:"requestId"`
	RequestTime      string `json:"requestTime"`
	State            string `json:"state"`
}

/*
ActiveRequestInfo Active queries Request info v2
*/
type ActiveRequestInfo struct {
	ElapsedTime      string `json:"elapsedTime"`
	ExecutionTime    string `json:"executionTime"`
	RequestStatement string `json:"requestStatement"`
	RequestID        string `json:"requestId"`
	RequestTime      string `json:"requestTime"`
	State            string `json:"state"`
}

/*
QuerySummaryInfo Query summary info
*/
type QuerySummaryInfo struct {
	Threads                  int32   `json:"threads"`
	Cores                    int32   `json:"cores"`
	GCPausePercent           float32 `json:"gcPausePercent"`
	RequestCompleted         int64   `json:"completedRequests"`
	RequestActive            int64   `json:"currentActive"`
	RequestPreparedStatement int64   `json:"preparedStatementPercent"`
}

/*
BucketInfo Bucket basic info
*/
type BucketInfo struct {
	Name             string  `json:"name"`
	RAM              int64   `json:"ram"`
	QuotaPercentUsed float32 `json:"quotaPerentUsed"`
	OpsPerSecond     int64   `json:"opsPerSecond"`
	DiskFetches      int64   `json:"diskFetches"`
	Items            int64   `json:"items"`
	DiskUsed         int64   `json:"diskUsed"`
	MemUsed          int64   `json:"memUsed"`
	Threads          int32   `json:"threads"`
	Replicas         int32   `json:"replicas"`
}

/*
IndexResponse Index service response
*/
type IndexResponse struct {
	StorageMode string `json:"storageMode"`
	Progress    int32  `json:"progress"`
	Definition  string `json:"definition"`
	Status      string `json:"status"`
	Bucket      string `json:"bucket"`
	Index       string `json:"index"`
}

/*
IndexResponseWrapper Collection of index responses
*/
type IndexResponseWrapper struct {
	Indexes []IndexResponse `json:"indexes"`
}

func remap(services []string) ([]string, NodeInfo) {
	size := len(services)
	var nodeInfo NodeInfo
	roles := make([]string, size, size)
	ndx := 0
	for service := range services {
		if services[service] == "index" {
			nodeInfo.IsIndex = true
			roles[ndx] = "Index"
		}
		if services[service] == "kv" {
			nodeInfo.IsData = true
			roles[ndx] = "Data"
		}
		if services[service] == "n1ql" {
			nodeInfo.IsQuery = true
			roles[ndx] = "Query"
		}
		ndx++
	}
	nodeInfo.Services = roles
	return roles, nodeInfo
}

func getResponse(url string, user string, password string) (*http.Response, error) {
	client := &http.Client{
		Timeout: *clientTimeout,
	}
	req, _ := http.NewRequest("GET", url, nil)
	req.SetBasicAuth(user, password)
	return client.Do(req)
}

func getBucketStats(clusterName string, cluster string, user string, password string, timestamp int64) {
	api := "/pools/default/buckets?basic_stats=true&skipMap=true"
	url := "http://" + cluster + api
	resp, err := getResponse(url, user, password)
	if err != nil {
		log.Printf("Error getting bucket stats for cluster %s", cluster)
		log.Print(err)
	} else {
		defer resp.Body.Close()
		var buckets []BucketStatsResponse
		json.NewDecoder(resp.Body).Decode(&buckets)
		size := len(buckets)
		bucketsDoc := make([]BucketInfo, size, size)
		for i := range buckets {
			var bucket BucketInfo
			b := buckets[i]
			bucket.DiskFetches = b.BasicStats.DiskFetches
			bucket.DiskUsed = b.BasicStats.DiskUsed
			bucket.Items = b.BasicStats.Items
			bucket.MemUsed = b.BasicStats.MemUsed
			bucket.Name = b.Name
			bucket.OpsPerSecond = b.BasicStats.OpsPerSecond
			bucket.QuotaPercentUsed = b.BasicStats.QuotaPercentUsed
			bucket.RAM = b.Quota.RAM
			bucket.Replicas = b.ReplicaNumber
			bucket.Threads = b.ThreadsNumber
			bucketsDoc[i] = bucket
		}
		saveStats(clusterName, "bucketsStats", bucketsDoc, timestamp)
	}
}

func scrapAdminQuery(clusterName string, hostname string, cluster string, user string, password string, timestamp int64) {
	api := ":8093/admin/vitals"
	url := "http://" + hostname + api
	resp, err := getResponse(url, user, password)
	if err != nil {
		log.Printf("Error reading N1QL APIs from %s", cluster)
		log.Print(err)
	} else {
		defer resp.Body.Close()
		var queryAdminResponse QueryAdminResponse
		var queryInfo QuerySummaryInfo
		json.NewDecoder(resp.Body).Decode(&queryAdminResponse)
		queryInfo.Cores = queryAdminResponse.Cores
		queryInfo.GCPausePercent = queryAdminResponse.GCPausePercent
		queryInfo.RequestActive = queryAdminResponse.RequestActive
		queryInfo.RequestCompleted = queryAdminResponse.RequestCompleted
		queryInfo.RequestPreparedStatement = queryAdminResponse.RequestPreparedStatement
		queryInfo.Threads = queryAdminResponse.Threads
		go saveStats(clusterName, "queryStats", queryInfo, timestamp)
	}
}

func scrapActiveQueries(clusterName string, hostname string, cluster string, user string, password string, timestamp int64) {
	api := ":8093/admin/active_requests"
	url := "http://" + hostname + api
	resp, err := getResponse(url, user, password)
	if err != nil {
		log.Printf("Error reading Active running queries APIs from %s", cluster)
		log.Print(err)
	} else {
		defer resp.Body.Close()
		var activeQueriesResponse []ActiveQueriesResponse
		json.NewDecoder(resp.Body).Decode(&activeQueriesResponse)
		size := len(activeQueriesResponse)
		activeRequestInfoCollection := make([]ActiveRequestInfo, size, size)
		for i := range activeQueriesResponse {
			var activeRequestInfo ActiveRequestInfo
			current := activeQueriesResponse[i]
			activeRequestInfo.ElapsedTime = current.ElapsedTime
			activeRequestInfo.ExecutionTime = current.ExecutionTime
			activeRequestInfo.RequestID = current.RequestID
			activeRequestInfo.RequestStatement = current.RequestStatement
			activeRequestInfo.RequestTime = current.RequestTime
			activeRequestInfo.State = current.State
			activeRequestInfoCollection[i] = activeRequestInfo
		}
		go saveStats(clusterName, "queryActiveStats", activeRequestInfoCollection, timestamp)
	}
}

func scrapQueryAPIs(clusterName string, cluster string, user string, password string, timestamp int64) {
	hostname := strings.Replace(cluster, ":8091", "", 1)
	go scrapAdminQuery(clusterName, hostname, cluster, user, password, timestamp)
	go scrapActiveQueries(clusterName, hostname, cluster, user, password, timestamp)
}

func getIndexes(clusterName string, cluster string, user string, password string, timestamp int64) {
	api := "/indexStatus"
	url := "http://" + cluster + api
	resp, err := getResponse(url, user, password)
	if err != nil {
		log.Printf("Error obtaining indexes %s", cluster)
		log.Print(err)
	} else {
		defer resp.Body.Close()
		var indexResponseWrapper IndexResponseWrapper
		json.NewDecoder(resp.Body).Decode(&indexResponseWrapper)
		indexes := indexResponseWrapper.Indexes
		go saveStats(clusterName, "indexStats", indexes, timestamp)
	}
}

func scrapCluster(clusterName string, cluster string, user string, password string, timestamp int64) {
	api := "/pools/default"
	url := "http://" + cluster + api
	start := time.Now()
	resp, err := getResponse(url, user, password)
	if err != nil {
		log.Printf("Error discovering %s", cluster)
		log.Print(err)
	} else {
		defer resp.Body.Close()
		took := time.Since(start)
		go getBucketStats(clusterName, cluster, user, password, timestamp)
		go getIndexes(clusterName, cluster, user, password, timestamp)
		log.Printf("Took %s with resp code %s", took, resp.Status)
		var clusterResponse ClusterResponse
		json.NewDecoder(resp.Body).Decode(&clusterResponse)
		for i := range clusterResponse.Nodes {
			var nodeInfo NodeInfo
			hostname := strings.Replace(clusterResponse.Nodes[i].Hostname, ":8091", "", 1)
			thisNode := clusterResponse.Nodes[i]
			thisNode.Services, nodeInfo = remap(clusterResponse.Nodes[i].Services)
			nodeInfo.Host = hostname
			nodeInfo.Version = thisNode.Version
			uptime, _ := strconv.ParseInt(thisNode.Uptime, 10, 64)
			nodeInfo.Uptime = uptime
			nodeInfo.CPUUsed = thisNode.SystemStats.CPUUtilizationRate
			nodeInfo.MemoryTotal = thisNode.SystemStats.MemTotal
			nodeInfo.MemoryFree = thisNode.SystemStats.MemFree
			nodeInfo.MemoryUsed = nodeInfo.MemoryTotal - nodeInfo.MemoryFree
			go saveStats(clusterName, "nodeStats", nodeInfo, timestamp)
			if nodeInfo.IsData {

			}
			if nodeInfo.IsIndex {

			}
			if nodeInfo.IsQuery {
				go scrapQueryAPIs(clusterName, cluster, user, password, timestamp)
			}
		}
	}
}
