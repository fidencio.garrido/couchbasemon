package main

import (
	"log"
	"time"
)

var scrapTicker time.Ticker

func startMonitor() {
	log.Print("Starting monitor")
	duration := time.Duration(configuration.Interval) * time.Millisecond
	log.Print(duration)
	scrapTicker := time.NewTicker(duration)
	go func() {
		for t := range scrapTicker.C {
			scrap(t.Unix())
		}
	}()
}

func scrap(time int64) {
	for i := 0; i < len(configuration.Monitors); i++ {
		monitor := configuration.Monitors[i]
		if monitor.Active {
			log.Printf("Reaching out to cluster %s at %s", monitor.Name, monitor.Cluster)
			go scrapCluster(monitor.Name, monitor.Cluster, monitor.User, monitor.Password, time)
		}
	}
}
