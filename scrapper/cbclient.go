package main

import (
	"log"

	"github.com/couchbase/gocb"
	"github.com/google/uuid"
)

var bucket *gocb.Bucket

/*
StatsRecord Summarized version for storage
*/
type StatsRecord struct {
	Timestamp int64       `json:"timestamp"`
	DocType   string      `json:"docType"`
	Cluster   string      `json:"cluster"`
	MonitorID string      `json:"monitorId"`
	Document  interface{} `json:"document"`
}

func connectDB() {
	if configuration.Store.Server != "" {
		connString := "couchbase://" + configuration.Store.Server
		cluster, err := gocb.Connect(connString)
		if err != nil {
			log.Fatal("Cannot connect to DB to store stats")
		}
		tmpbucket, berr := cluster.OpenBucket(configuration.Store.Bucket, configuration.Store.Password)
		if berr != nil {
			log.Fatal("Cannot connect to specified bucket " + configuration.Store.Bucket + " to store stats")
		}
		log.Print("Connected to DB")
		bucket = tmpbucket
	} else {
		readOnlyMode = true
		log.Print("Read only mode on")
	}
}

func saveStats(cluster string, docName string, document interface{}, ts int64) {
	if readOnlyMode == false {
		var record StatsRecord
		record.DocType = docName
		record.Document = document
		record.MonitorID = monitorID
		record.Cluster = cluster
		record.Timestamp = ts
		id := uuid.New().String()
		bucket.Insert(id, record, configuration.Store.RawStatsTTL)
		log.Printf("Saved %s@%s", docName, cluster)
	}
}
